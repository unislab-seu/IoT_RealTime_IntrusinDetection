# IoT_RealTime_IntrusinDetection
### 介绍
在本项目中利用流水线机制，构建了RTP-IDS（Real-time Pipeline Intrusion Detection System），依次对捕获窗口的流量数据进行分析。这样可以避免串行机制导致的延迟，因为RTP-IDS将实时入侵检测任务划分为不同的阶段，每个阶段并行执行，后一轮到达的流量数据不再需要等待前一轮检测流程的结束便可同时进行。同时该方法考虑了物联网设备在日常使用时的网络流量情况，设计了固定捕获数据块大小的捕获策略。在数据预处理阶段使用NFStream进行数据处理来获得详细的设备行为特征向量，模型决策阶段使用LightGBM(Light Gradient Boosting Machine)作为入侵检测算法，提升检测准确度和效率。

该项目运行流程如图所示：

![输入图片说明](.idea/%E7%BD%91%E9%80%9F%E5%9D%87%E5%8C%80%E6%97%B6%E6%8D%95%E8%8E%B7%E7%9A%84%E6%B5%81%E7%A8%8B.png)

RTP-IDS分为四个阶段：数据捕获阶段、数据预处理阶段、模型决策阶段和告警与记录阶段。

1.  数据捕获阶段：负责实时数据的捕获，捕获方式为固定数据块大小。
2.  数据预处理阶段：使用NFStream进行原始数据流量的特征提取。
3.  模型决策阶段：使用训练好的Lightgbm快速执行分类决策。
4.  告警与记录阶段：上报并记录异常。

### 软件架构

本项目总共包括四个模块：

1.  数据包捕获模块——Tshark   （位于FlowGeneratorwen<..>.py内）
2.  数据预处理模块——NFStream （位于preparedumps、preprocessing文件夹内）
3.  模型决策阶段——Lightgbm （位于ml_models文件夹、MachineProcessor.py内）
4.  告警与记录模块 （位于logs文件夹内）


### 依赖清单
| 序号 |    安装模块        |
|----|-----------------------|
|1| joblib==0.17.0
|2|libpcap==1.11.0b7|
|3|lightgbm==3.3.5|
|4|matplotlib==3.3.2|
|5|netifaces==0.10.9|
|6|nfstream==6.5.3|
|7|numpy==1.19.2|
|8|pandas==1.5.3|
|9|pyshark==0.5.3|
|10|scapy==2.4.3|
|11|scikit_learn==1.2.2|

### 使用说明

1.  运行FlowGenerator_livecapture_wire_process_time.py，输入自定义参数并运行。
2.  在capture文件夹下是捕获的原始pcap文件。
3.  观察logs文件夹下的记录文件。


